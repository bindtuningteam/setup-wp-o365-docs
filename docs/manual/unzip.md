Any Web Part is provided in 3 different packages, all following Microsoft guidelines:

For **Classic Experience**: 

- ***Add-in***, for tenants with a configured App Store/Catalog.
- ***WSP***, no-code sandbox solution, (**recommended**) whenever sandbox is available.

For **Modern Experience**: 

- ***SPFx***, SharePoint Modern Pages.

After unzipping your Web Part package, you will find three folders:
- *App*;
- *Wsp*;
- *Spfx*

You will be using one of these folders to install the Web Part.

But first, let's check out what is inside the package.

![Zip structure.PNG](https://bitbucket.org/repo/daEeqRX/images/495949209-Zip%20structure.PNG)

-------------
**app** 📁

└── BT _x.x.x.x.app 

---------

**spfx** 📁

└── SP2019

└────── BT _x.x.x.x.sppkg

└── Office365

└────── BT_* MSTeams.zip

└────── BT* _x.x.x.x.sppkg
   
-------------
**wsp (Recommended)** 📁

└──  BT _x.x.x.x.wsp

---------

└── README.md