To manually install your Web Part(s), you'll have to **download** the corresponding package.

1. Access your account at <a href="http://bindtuning.com" target="_blank">BindTuning</a>;
2. Go to **Build** feature tab.
3. Mouse over the Web part and click on **More Details**.

    ![manual-install-1.png](../images/manual-install-1.png)

4. Last but not least, click on **Download**.

    ![Download.PNG](../images/manual-download.png)