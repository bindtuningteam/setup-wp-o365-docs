To completely remove a Web Part from SharePoint (and not just uninstall it) you must follow the instructions below:

1. Go to **Site contents** of your root Site Collection and click on **Style Library**;

    ![Style Library.PNG](https://bitbucket.org/repo/daEeqRX/images/2105588219-Style%20Library.PNG)

2. Search for **BT WebParts Add-ins** and open the folder;
3. Select the Web Part(s) that you intend to remove;
4. Click on the option to **Delete**.

    ![Remove Files.PNG](https://bitbucket.org/repo/daEeqRX/images/1032215659-Remove%20Files.PNG)

The BindTuning Web Part(s) files had been removed from the site.