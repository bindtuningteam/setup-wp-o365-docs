Have you purchased the BindTuning Web Part(s)? If so, you need to proceed as follow to make sure the latest files version are updated on your pages.

<p class="alert alert-info">BindTuning recommends to use the Google Chrome for this update process.</p>

1. <a href="https://bindtuning-setup-office-365-web-parts-guide.readthedocs.io/en/latest/manual/classic%20experience/add-in/update/" target="_blank">Follow the guide</a> to **Update the BindTuning Web Part**;
2. Open the page(s) where you have the web part(s);   
3. Click to **Edit** that page;
    
    ![Edit Mode.PNG](https://bitbucket.org/repo/daEeqRX/images/67258642-Edit%20Mode.PNG)

4. Click on the **Edit Web Parts** button under the **BindTuning ribbon** tab;

    ![Edit Mode click.PNG](https://bitbucket.org/repo/daEeqRX/images/115154009-Edit%20Mode%20click.PNG)

5. Do a **CTRL + F5** to make you clear the browser cache;
6. **Save** the page.

    ![Save.PNG](https://bitbucket.org/repo/daEeqRX/images/3819357660-Save.PNG)

The Web parts are now in the full licensed version for your SharePoint.
