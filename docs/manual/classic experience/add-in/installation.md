If you want to use the SharePoint Add-in model to install the web part on your website, then you are in the right place. 😉

### Install Add-in on APP catalog 

<p class="alert-success">The steps in this section need to be done only <strong>once per SharePoint instance</strong> by a global <strong>SharePoint Administrator</strong>. After this initial installation, the web parts will become available for installation in each site collection.</p> 

#### Office 365

1. Login to your Office 365 admin center page with an administrator account;
1. Open the app launcher and click on **Admin**;

	![Admin Center.png](https://bitbucket.org/repo/daEeqRX/images/1075500318-Admin%20Center.png)
	
1. Open your SharePoint **Admin Center**; 

	![open-sharepoint-admin-center.png](../../../images/open-sharepoint-admin-center.png)

1. On the left panel, click on **More features** and select **Apps**;

	![open-app-catalog.png](../../../images/open-app-catalog.png)
	
1. Proceed by clicking on **App Catalog**;

1. On the left pane, click on **Apps for SharePoint**;

	![APPs for SharePoint.png](https://bitbucket.org/repo/6499g4B/images/2864249072-APPs%20for%20SharePoint.png)
	
1. Now click on **Upload** and upload the app file that is inside your web part package;

	![Install.gif](https://bitbucket.org/repo/daEeqRX/images/2576993045-Install.gif)

Now you can install the  web part in every site collection in your SharePoint tenant. And that's what you are going to do next! 🙂

-------
### Install the web part on a SharePoint Site

<p class="alert-success">The steps in this section need to be done for <strong>each site collection</strong> where you want to use the web part by a <strong>Site Collection Administrator</strong>.</p>

1. Open the Site Collection where you want your web part installed, and select **Add an App**;

	![Add an APP.PNG](https://bitbucket.org/repo/6499g4B/images/4002785227-Add%20an%20APP.PNG)
		
2. Search for **bt** in the search box and select the web part app;

	![Search_BT.PNG](https://bitbucket.org/repo/daEeqRX/images/3446996563-Search_BT.PNG)

3. Click on the **Trust It** button and wait for the app to install - you might need to wait for a few minutes;

	![Trust_IT.PNG](https://bitbucket.org/repo/daEeqRX/images/3669985873-Trust_IT.PNG)

4. Proceed to your **Site Contents**. The installation will proceed **automatically**.

5. After the installation has finished, you see the added **BindTuning Add-ins** in a bright blue color.

	![add-in-installed.png](../../../images/add-in-installed.png)

Web part installed! ✅

-------

Done! To complete the setup process the only thing left to do is to add it to the page and configure all its properties.