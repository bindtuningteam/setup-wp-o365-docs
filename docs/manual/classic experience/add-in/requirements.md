### Platform version
- Office 365 (SharePoint Online) 

-------
### Browsers

BindTuning Web Part(s) work in all modern browsers:

- Firefox
- Chrome
- Safari
- Opera
- Internet Explorer 11
- Edge

-------
### SharePoint requirements 

- **SharePoint Online** with **App Catalog**. Please have a look at the <a href="https://support.bindtuning.com/hc/en-us/articles/360027696532" target="_blank">next article</a> to activate/configure this option;
- Access to **App Catalog** with Administration rights;
- **Site Collection admin** rights;
- An active **BindTuning Web Parts** subscription or trial.