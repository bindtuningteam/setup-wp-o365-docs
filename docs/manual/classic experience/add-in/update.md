### Update Add-in on APP catalog 

<p class="alert-success">The steps in this section need to be done only <strong>once per SharePoint instance</strong> by a global <strong>SharePoint Administrator</strong>. After this initial installation, the web parts will become available for installation in each site collection.</p> 

#### Office 365

1. Login to your Office 365 admin center page with an administrator account;
1. Open the app launcher and click on **Admin**; 	
1. Open your SharePoint Admin Center; 

	![Admin Center.png](https://bitbucket.org/repo/daEeqRX/images/1075500318-Admin%20Center.png)
	
1. Open your SharePoint **Admin Center**; 

	![open-sharepoint-admin-center.png](../../../images/open-sharepoint-admin-center.png)

1. On the left panel, click on **More features** and select **Apps**;

	![open-app-catalog.png](../../../images/open-app-catalog.png)
	
1. Proceed by clicking on **App Catalog**;

1. On the left pane, click on **Apps for SharePoint**;

	![APPs for SharePoint.png](https://bitbucket.org/repo/6499g4B/images/2864249072-APPs%20for%20SharePoint.png)
	
1. Now click on **Upload** and upload the app file that is inside your web part package;
		
	![App Catalog.PNG](https://bitbucket.org/repo/6499g4B/images/102677999-App%20Catalog.PNG)

1. You will be prompted to replace the existing file on the library. Click **Yes**.


-----
### Update the web part on Site

<p class="alert-success">The steps in this section need to be done for <strong>each site collection</strong> where you want to use the web part by a <strong>Site Collection Administrator</strong>.</p>

1. Inside your site collection, click on **Settings** ⚙️ and then on **Site contents**; 
	
	![Site Contents.PNG](https://bitbucket.org/repo/6499g4B/images/1722948873-Site%20Contents.PNG)
	
2. Click on the ellipsis button on the APP and then **About**; 

	![Update About.PNG](https://bitbucket.org/repo/daEeqRX/images/4135262513-Update%20About.PNG)

3. You will see a new version available. Click on **Get It** button and wait for the app to finish updating;

	![Get it Update.png](https://bitbucket.org/repo/daEeqRX/images/2346472233-Get%20it%20Update.png)

5. Click on **Trust It** and wait for the app to finish updating;

6. Once the update is complete, click on the APP and them click on **Update Available**;

![Update available..png](https://bitbucket.org/repo/daEeqRX/images/1923184315-Update%20available..png)

7. The page will begin to install a new version of the web part into your Site Collection.

-----

The page will begin re-deploying the web part into your site. Once it’s finished you will have the new version on you site collection.