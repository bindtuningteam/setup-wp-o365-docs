If you want to use the SharePoint **Sandbox** solution to install the Web Part(s) on your website, then you are in the right place. 😉

1. Open the settings menu (⚙) and click on **Site settings**;

	![SiteSettings.PNG](https://bitbucket.org/repo/daEeqRX/images/3912387186-SiteSettings.PNG)

2. Under **Web Designer Galleries**, click on **Solutions**;
3. On the tool bar click **Upload Solution** and select the installation **.wsp** file (eg. BT*XXX_x.x.x.x*.wsp);

	![Install WSP.gif](https://bitbucket.org/repo/daEeqRX/images/589981505-Install%20WSP.gif)
	
4. As soon as the upload is complete, click on **Activate** and wait for the page to refresh.

	![Activate WSP.png](https://bitbucket.org/repo/daEeqRX/images/2533301188-Activate%20WSP.png)

Web Part installed! ✅ 

-----

Done! To complete the setup process the only thing left to do now is add it to the page and configure all its properties.