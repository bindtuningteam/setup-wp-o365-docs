1. Open **Site Settings** at the root of your site collection;

    ![SiteSettings.PNG](https://bitbucket.org/repo/daEeqRX/images/3912387186-SiteSettings.PNG)

2. Select **Solutions**, under Web Design Galleries;

    ![Solutions.png](https://bitbucket.org/repo/daEeqRX/images/3461179881-Solutions.png)

3. Select the already existing web part solution and **Deactivate** it;

    ![Deactivate WSP.gif](https://bitbucket.org/repo/daEeqRX/images/977372970-Deactivate%20WSP.gif)

4. Once it finishes deactivating, **Delete** the solution from the gallery.

    ![Delete WSP.gif](https://bitbucket.org/repo/daEeqRX/images/3466951891-Delete%20WSP.gif)

The BindTuning Web Part is now uninstalled from your SharePoint site.