To completely remove a Web Part from SharePoint (and not just uninstall it) you must follow the instructions below:

1. Go to **Site contents** and click on **Style Library**;
    
    ![Style Library.PNG](https://bitbucket.org/repo/daEeqRX/images/2105588219-Style%20Library.PNG)

2. Search for **BT WebParts Add-ins** and open the folder;
3. Select the Web Part(s) that you intend to remove;
4. Click on the option to **Delete**.

    ![Remove Files.PNG](https://bitbucket.org/repo/daEeqRX/images/1032215659-Remove%20Files.PNG)

Next, we'll have to delete the corresponding Web Parts from the **Web Part Gallery**: 

1. Go to **Site Settings**; 

1. Under **Web Designer Galleries** click on **Web parts**; 

    ![web-part-gallery](../../../images/web-part-gallery.png)

1. Select the Web Part(s) you want to remove; 

1. On the ribbon, click on **Files**; 

1. Select the option **Delete Document**.

    ![remove-document](../../../images/remove-document.png)

The BindTuning Web Part(s) files have been removed from the site! ✅ 