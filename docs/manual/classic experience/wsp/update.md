**Note:** Instructions on how to download the updated package can be found <a href="https://bindtuning-setup-office-365-web-parts-guide.readthedocs.io/en/latest/bindtuning/update/">here</a>.

1. Open **Site settings** at the root of your site collection; 	

	![SiteSettings.PNG](https://bitbucket.org/repo/daEeqRX/images/3912387186-SiteSettings.PNG)

2. Select **Solutions**, under **Web Design Galleries**;

	![Solutions.png](https://bitbucket.org/repo/daEeqRX/images/3461179881-Solutions.png)

3. Select the already existing Web Part solution and **Deactivate** it; 

	![Deactivate WSP.gif](https://bitbucket.org/repo/daEeqRX/images/977372970-Deactivate%20WSP.gif)	

4. Once it finishes deactivating, **Delete** the solution from the gallery;

5. On the tool bar click **Upload Solution** and select the installation **.wsp** file (ex. BT*XXX_x.x.x.x*.wsp)	

	![Install WSP.gif](https://bitbucket.org/repo/daEeqRX/images/589981505-Install%20WSP.gif)


6. As soon as the upload is complete, click on **Activate** and wait for the page to refresh.

	![Activate WSP.png](https://bitbucket.org/repo/daEeqRX/images/2533301188-Activate%20WSP.png)

Web Part upgraded! ✅