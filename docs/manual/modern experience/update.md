<p class="alert-success">If you know the URL to your App catalog or are using a site collection app catalog, you can <strong>skip to step 5</strong>.</p>

1. Open the app launcher and click on **Admin**;

	![Admin Center.png](https://bitbucket.org/repo/daEeqRX/images/1075500318-Admin%20Center.png)
	
1. Open your SharePoint **Admin Center**; 

	![open-sharepoint-admin-center.png](../../images/open-sharepoint-admin-center.png)

1. On the left panel, click on **More features** and select **Apps**;

	![open-app-catalog.png](../../images/open-app-catalog.png)
	
1. Proceed by clicking on **App Catalog**;

1. On the left pane, click on **Apps for SharePoint**;

	![APPs for SharePoint.png](https://bitbucket.org/repo/6499g4B/images/2864249072-APPs%20for%20SharePoint.png)

1. Select the older package of the **Web Part** deployed, click at the option **Files**, in ribbon, and click to **Delete Document**;

	![Delete files.gif](https://bitbucket.org/repo/daEeqRX/images/3435324203-Delete%20files.gif)

1. Now click on **Upload** and select the *.sppkg* file that's inside the **spfx** folder in your web part package;

	![Install SPFx.gif](https://bitbucket.org/repo/daEeqRX/images/2122301107-Install%20SPFx.gif)

    
1. Finally, on the pop up that appears, tick the checkbox **Make this solution available to all sites in the organization** and click **Deploy**.

    ![Trust and deploy.png](https://bitbucket.org/repo/6499g4B/images/2615707783-Trust%20and%20deploy.png)

And that's it! The web part is now updated for the latest version.