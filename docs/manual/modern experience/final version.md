Have you purchased the BindTuning Web Part(s)? If so, you need to proceed as follow to make sure the latest files version are updated on your pages.

1. <a href="https://bindtuning-setup-office-365-web-parts-guide.readthedocs.io/en/latest/manual/modern%20experience/update/" target="_blank">Follow the guide</a> to **Update the BindTuning Web Part**;
2. Do a **CTRL + F5** to make you clear the browser caching;

The Web parts are now in the full licensed version for your SharePoint.