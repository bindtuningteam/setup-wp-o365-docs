The **BindTuning online app** allows for the easy deployment of Web Parts on Office 365 (SharePoint Online). 

**Note**: This installation method is only valid for Office 365 (SharePoint Online).

1. Login to your BindTuning account;
1. Navigate to the **Build** tab; 

    ![build-tab.png](../../images/build-tab.png)

1. Mouse over the selected Web Part and click on **More Details**; 

1. Click on **Install**; 

1. Proceed with either:
    - The installation of the selected Web Part;
    - A bulk installation of all Web Parts.

    ![install-online-app.png](../../images/install-online-app.png)

1. Select the option **Office 365**;
1. Input both your Site Collection URL and corresponding Office 365 user email;

1. Select the **SharePoint experience** you want to deploy to, as well as correlative **installation scope**:
    
    - **Classic:** Deploys the theme on  **Classic SharePoint**, being available at the site collection level;
    - **Modern:** Deploys the theme on  **Modern SharePoint**, being available at either **site collection** or **tenant** level.

    ![install-site-collection.png](../../images/install-site-collection.png)

    **Note:** While the **site collection** level installation will **only** deploy the solutions to the URL provided in **step 7**, the **tenant wide** deployment will make the solutions available for **any** site collection belonging to your tenancy, **regardless of site collection URL** provided in the above-mentioned step.

1. Verify all the provided information and click **Confirm**;
    ![confirm-installation.png](../../images/confirm-installation.png)

1. The installation will proceed in the background. To review it's progress click on **Recent Activity** (graph icon).

After the installation has been completed, an alert will appear, informing you of its status.

![installation-status-message.png](../../images/installation-status-message.png)



