To update the BindTuning Web Parts on your SharePoint site you'll, first, need to update them on your BindTuning account.

1. Login on your **BindTuning account** at <a href="https://bindtuning.com/">BindTuning</a>;
2. Navigate to your **Build** tab;
3. If an update for the Web Part(s) is available, you'll be able to see a red circle.

    ![available-updates.png](../images/available-updates.png)

4. To update the Web Parts, click on the icon and select **Update Now**;

    ![update-one-webpart.png](../images/update-one-webpart.png)

5. If you intend to install the Web Parts on **Office365**, you can mouse over one of the web parts with available updates, click on **More Details**, select the Web Part(s) to be installed and, right before the installation process begins, proceed to update them.

    ![bulk-update.png](../images/bulk-update.png)

6. The update process will begin and you'll be notified once the update has finished. ✅